import mongoose from 'mongoose'

const TransactionSchema = new mongoose.Schema({
  amount: Number,
  description: String,
  category: String,
  type: String
})

export const Transaction = mongoose.models.Transaction || mongoose.model('Transaction', TransactionSchema)