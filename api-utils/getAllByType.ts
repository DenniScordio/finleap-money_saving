import { Type } from "../interfaces/Type";

export const getAllByType = async (type: Type) => {
    try {
        const response = await fetch(`/api/transactions/${type}`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json"
          }
        });
        const responseData = await response.json()
        return responseData.data;
      } catch (err) {
        console.log(err);
        return err;
      }
}
