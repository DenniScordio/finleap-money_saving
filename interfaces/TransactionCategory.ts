export enum IncomeCategory {
    Salaries = "SALARIES",
    Various = "VARIOUS"
}

export enum ExpenseCategory {
    Home = "HOME",
    Transport = "TRANSPORT",
    Family = "FAMILY",
    Health = "HEALTH",
    FreeTime = "FREETIME",
    Others = "OTHERS",
}
