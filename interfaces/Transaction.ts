import { IncomeCategory, ExpenseCategory } from "./TransactionCategory";
import { Type } from "./Type";

export default interface Transaction {
    id?: string;
    amount: number;
    description: string;
    type: Type;
    category: IncomeCategory | ExpenseCategory;
}