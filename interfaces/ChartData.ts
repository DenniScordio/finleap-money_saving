import { ExpenseCategory, IncomeCategory } from "./TransactionCategory";

export default interface ChartData {
    _id: IncomeCategory | ExpenseCategory;
    total: number;
}