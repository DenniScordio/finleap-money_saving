
import MainNavigation from './Header';

import styles from './Layout.module.scss';

const Layout = ({ children }: any) => {
  return (
    <div>
      <MainNavigation />
      <main className={styles.layout}>{children} </main>
    </div>
  );
};

export default Layout;
