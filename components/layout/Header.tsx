import Link from 'next/link';

import styles from './Header.module.scss';

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.logo}>My Wallet</div>
      <nav>
        <ul>
          <li>
            <Link href='/'>All Transactions</Link>
          </li>
          <li>
            <Link href='/new-transaction'>Add New Transactions</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default Header;
