import TransactionItem from './TransactionItem';
import Transaction from '../../interfaces/Transaction';

import styles from './TransactionList.module.scss';

const TransactionList = (props: { items: Array<Transaction> }) => {
    return (
        <div className={styles.transactions}>
            <div className={styles.transactionHeader}>
                <h4>Description</h4>
                <h4>Category</h4>
                <h4>Amount</h4>
            </div>
            {props.items.map((item: Transaction) => (
                <TransactionItem
                    key={item.id}
                    transaction={item}
                />
            ))}
            {!props.items || props.items.length === 0 &&
                <h4 className={styles.info}>You have not added any transactions yet, add one</h4>
            }
        </div>
    );
}

export default TransactionList;
