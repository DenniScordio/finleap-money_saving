import * as Yup from 'yup'

export const TransactionFormSchema = function () {
    return Yup.object().shape({
        description: Yup.string()
            .required('Description is required'),
        amount: Yup.number()
            .typeError('Amount is required')
            .min(0.01, 'Min value is 0.01'),
        type: Yup.string()
            .required('Type is required'),
        category: Yup.string()
            .required('Category is required'),
    })
}