import Transaction from '../../interfaces/Transaction';

import styles from './TransactionItem.module.scss';

const TransactionItem = ({ transaction }: { transaction: Transaction }) => {
    return (
        <div className={styles.transaction}>
            <h3>{transaction.description}</h3>
            <h3 className={styles.category}>{transaction.category}</h3>
            <h3 className={styles.amount}>{transaction.type === "INCOME" ? `+ ${transaction.amount}` : `- ${transaction.amount}`}€</h3>
        </div>
    );
}

export default TransactionItem;
