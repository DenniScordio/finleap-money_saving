import * as React from 'react';
import { Formik, Form } from 'formik';
import { Type } from '../../interfaces/Type';
import { ExpenseCategory, IncomeCategory } from '../../interfaces/TransactionCategory';
import { validate } from '../../utils/validate';
import { TransactionFormSchema } from './TransactionFormSchema';

import styles from './TransactionForm.module.scss';

const NewTransactionForm = ({ addTransaction }: any) => {

    const onSubmit = (values: any) => {
        addTransaction(values);
    }

    const initialValues = {
        amount: "",
        description: "",
        category: "",
        type: ""
    };

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validate={validate(TransactionFormSchema)}
        >
            {formik => {
                const { touched, errors, values, handleChange, handleSubmit, handleBlur, isValid, dirty, isSubmitting, setFieldValue } = formik
                return (
                    <Form className={styles.form} onSubmit={handleSubmit} autoComplete="off">
                        <h1>New Transaction</h1>
                        <div className={styles.control}>
                            <label htmlFor='description'>Description</label>
                            <input type='text' value={values.description} onChange={handleChange} onBlur={handleBlur} required id='description' />
                            {touched.description && errors.description && <div className={styles.error}>{errors.description}</div>}
                        </div>
                        <div className={styles.control}>
                            <label htmlFor='amount'>Amount</label>
                            <input type='number' value={values.amount} onChange={handleChange} onBlur={handleBlur} required id='amount' />
                            {touched.amount && errors.amount && <div className={styles.error}>{errors.amount}</div>}
                        </div>
                        <div className={styles.control}>
                            <label htmlFor='type'>Type</label>
                            <select value={values.type} id='type'
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    setFieldValue("type", e.target.value)
                                    setFieldValue("category", "")
                                    handleChange(e)
                                }}>
                                <option key="" disabled value="">Select a Type</option>
                                {Object.keys(Type).map(key => (
                                    <option key={key} value={key.toUpperCase()}>
                                        {key}
                                    </option>
                                ))}
                            </select>
                            {touched.type && errors.type && <div className={styles.error}>{errors.type}</div>}
                        </div>
                        <div className={styles.control}>
                            {values.type &&
                                <label htmlFor='category'>Category</label>
                            }
                            {values.type === Type.Income &&
                                <select value={values.category} onChange={handleChange} id='category'>
                                    <option key="" disabled value="">Select a category</option>
                                    {Object.keys(IncomeCategory).map(key => (
                                        <option key={key} value={key.toUpperCase()}>
                                            {key}
                                        </option>
                                    ))}
                                </select>
                            }
                            {values.type === Type.Expense &&
                                <select value={values.category} onChange={handleChange} id='category'>
                                    <option key="" disabled value="">Select a category</option>
                                    {Object.keys(ExpenseCategory).map(key => (
                                        <option key={key} value={key.toUpperCase()}>
                                            {key}
                                        </option>
                                    ))}
                                </select>
                            }
                            {touched.category && errors.category && <div className={styles.error}>{errors.category}</div>}
                        </div>
                        <div className={styles.actions}>
                            <button type='submit'
                                disabled={!(isValid && dirty) || isSubmitting}>
                                Add Transaction
                            </button>
                        </div>
                    </Form>
                )
            }}
        </Formik>
    )
};

export default NewTransactionForm;