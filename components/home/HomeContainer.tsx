import Transaction from '../../interfaces/Transaction';
import Balance from '../balance/Balance';
import TransactionList from '../transactions/TransactionList';
import ChartContainer from '../charts/ChartContainer';

import styles from './HomeContainer.module.scss';

const HomeContainer = (props: { items: Array<Transaction> }) => {
    return (
        <div className={styles.home}>
            <div>
                <Balance items={props.items} />
                <hr />
                <TransactionList items={props.items} />
            </div>
            <ChartContainer />
        </div>
    );
}

export default HomeContainer;
