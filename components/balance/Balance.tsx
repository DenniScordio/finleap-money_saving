import { useState, useEffect } from 'react';
import Transaction from '../../interfaces/Transaction';
import { Type } from '../../interfaces/Type';

import styles from './Balance.module.scss';

const Balance = (props: { items: Array<Transaction> }) => {
    const { items } = props;
    const [balance, setBalance] = useState<number>(0);
    useEffect(() => {
        let total = 0;
        items.forEach((transaction: Transaction) => {
            if (transaction.type === Type.Income) {
                total = total + transaction.amount;
            } else {
                total = total - transaction.amount
            }
        })
        setBalance(total);
    }, [items])

    return (
        <div className={`${styles["balance"]} ${balance > 0 ? styles["positive"] : styles["negative"]}`}>
            Your balance is: <b>{balance.toFixed(2)}€</b>
        </div>
    );
}

export default Balance;
