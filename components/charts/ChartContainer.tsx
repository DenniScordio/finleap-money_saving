import Chart from './Chart';
import { Type } from '../../interfaces/Type';

import styles from './ChartContainer.module.scss';

const ChartContainer = () => {
    return (
        <div className={styles.charts}>
            <Chart type={Type.Expense} />
            <hr />
            <Chart type={Type.Income} />
        </div>
    );
}

export default ChartContainer;
