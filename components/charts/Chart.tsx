import { useEffect, useState } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Type } from '../../interfaces/Type';
import ChartData from '../../interfaces/ChartData';
import { Doughnut } from 'react-chartjs-2';
import { getAllByType } from '../../api-utils/getAllByType';
import { ExpenseCategory, IncomeCategory } from '../../interfaces/TransactionCategory';

import styles from './Chart.module.scss';

ChartJS.register(ArcElement, Tooltip, Legend);

const Chart = ({ type }: { type: Type }) => {

    const [values, setValues] = useState<ChartData[]>();
    const [data, setData] = useState<number[]>();
    const [labels, setLabels] = useState<Array<IncomeCategory | ExpenseCategory>>();

    useEffect(() => {
        getAllByType(type)
            .then(response => {
                prepareData(response)
            }).catch(error => {
                console.log(error);
            })
    }, [type])

    const prepareData = (values: ChartData[]) => {
        let labels: Array<IncomeCategory | ExpenseCategory> = []
        let datasets: number[] = []
        values?.forEach((item: ChartData) => {
            labels.push(item._id)
            datasets.push(item.total)
        })
        setValues(values)
        setData(datasets)
        setLabels(labels);
    }

    const chartData = {
        labels: labels,
        datasets: [
            {
                data: data,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(255, 99, 132, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                ],
                borderWidth: 1,
            },
        ],
    };
    if (values && values.length > 0) {
        return (
            <div className={styles.chart}>
                <h3>{type}</h3>
                <Doughnut data={chartData} />
            </div>
        );
    } else return null;

}

export default Chart;
