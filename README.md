## Getting Started with Docker

Run docker-compose up to create and start containers (if you have mongodb active on port 27017, stop it before running the command).

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Getting Started without Docker

If it is not already installed, install mongodb using Homebrew (follow this guide: [https://docs.mongodb.com/manual/administration/install-community/](https://docs.mongodb.com/manual/administration/install-community/)

Then, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

