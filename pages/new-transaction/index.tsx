import type { NextPage } from 'next'
import { useRouter } from "next/router";
import TransactionForm from '../../components/transactions/TransactionForm';
import Transaction from '../../interfaces/Transaction';

const NewTransactionPage: NextPage = () => {
  const router = useRouter();

  async function addTransaction(data: Transaction) {
    try {
      const response = await fetch("/api/transactions", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
          "Content-Type": "application/json"
        }
      });
      const responseData = await response.json()
      console.log(responseData)
      router.push("/")
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <TransactionForm addTransaction={addTransaction} />
  )
}

export default NewTransactionPage