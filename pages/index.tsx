import type { NextPage, GetStaticProps } from 'next'
import Transaction from '../interfaces/Transaction'
import HomeContainer from '../components/home/HomeContainer'

interface HomeProps {
  items: Transaction[];
}

const Home: NextPage<HomeProps> = ({ items }) => {
  return (
    <HomeContainer items={items} />
  )
}

export const getStaticProps: GetStaticProps = async () => {
  let transactions = [];
  try {
    const response = await fetch("http://localhost:3000/api/transactions", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    });
    const responseData = await response.json()
    transactions = responseData.data;
  } catch (err) {
    console.log(err);
  }

  return {
    props: {
      items: transactions.map((item: any) => ({
        amount: item.amount,
        description: item.description,
        category: item.category ? item.category : null,
        type: item.type,
        id: item?._id.toString()
      }))
    },
    revalidate: 1
  }
}

export default Home

