import type { NextApiRequest, NextApiResponse } from 'next'
import { connect } from '../../../utils/connection'
import { Transaction } from '../../../models/mongodb/TransactionSchema';
import mongoose from 'mongoose'

type ResponseData = {
  success: boolean,
  data?: any,
  error?: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {
  const { type } = req.query;

  await connect();
  try {
    const transactions = await Transaction.aggregate([
      { $match: { type: type } },
      {
        $group: {
          _id: "$category",
          total: { $sum: "$amount" }
        }
      }
    ])

    res.status(200).json({ success: true, data: transactions })
  } catch (error) {
    res.status(400).json({ success: false, error: error })
  }
}
