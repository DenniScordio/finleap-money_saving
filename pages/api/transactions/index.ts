import type { NextApiRequest, NextApiResponse } from 'next'
import { connect } from '../../../utils/connection'
import { Transaction } from '../../../models/mongodb/TransactionSchema';
import mongoose from 'mongoose'

type ResponseData = {
  success: boolean,
  data?: any,
  error?: any
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<ResponseData>
) {
  const { method } = req

  await connect();

  switch (method) {
    case 'GET':
      try {
        const transactions = await Transaction.find({})
        console.log(transactions);
        res.status(200).json({ success: true, data: transactions })
      } catch (error) {
        res.status(400).json({ success: false, error: error })
      }
      break
    case 'POST':
      try {
        const meetup = await Transaction.create(req.body)
        res.status(201).json({ success: true, data: meetup })
      } catch (error) {
        res.status(400).json({ success: false, error: error })
      }
      break
    default:
      res.status(400).json({ success: false })
      break
  }
}
