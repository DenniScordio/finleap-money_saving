describe('Test navigation and functionality', () => {
    beforeEach(() => {
        // Start from the index page
        cy.visit('http://localhost:3000/')
    })

    it('should add a new transaction', () => {

        cy.contains('Add New Transaction').click()

        // The new url should include "/new-transaction"
        cy.url().should('include', '/new-transaction')

        // The button should be disabled
        cy.get('button').should('be.disabled')

        cy.get('#description')
            .type('Test transaction').should('have.value', 'Test transaction')

        cy.get('#amount')
            .type(50).should('have.value', 50)

        cy.get('#type').select('EXPENSE').should('have.value', 'EXPENSE')

        cy.get('#category').select('HOME').should('have.value', 'HOME')

        // The button now should be enabled
        cy.get('button').should('be.enabled')

        cy.get('form').submit()

        // The new url should be "/"
        cy.url().should('eq', 'http://localhost:3000/')

        // check if the added transaction is present in the list
        cy.contains('Test transaction')
    })
})