describe('Test navigation and functionality', () => {
    beforeEach(() => {
        // Start from the index page
        cy.visit('http://localhost:3000/')
    })

    it('should navigate to the new transaction page', () => {

        cy.contains('Add New Transaction').click()

        // The new url should include "/about"
        cy.url().should('include', '/new-transaction')

        // The new page should contain an h1 with "New Transaction"
        cy.get('h1').contains('New Transaction')
    })
})