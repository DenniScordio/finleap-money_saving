import mongoose from 'mongoose'

const MONGODB_URI = process.env.MONGODB_URI

if (!MONGODB_URI) {
  throw new Error(
    'Please define the MONGODB_URI environment variable'
  )
}

// connection function
export const connect = async () => {
  try {
    const connection = await mongoose.connect(MONGODB_URI as string)
    return connection
  } catch (err) {
    console.log("Connection error");
    console.log(err);
  }
}